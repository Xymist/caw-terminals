**README**

This repository contains the files used for the interfaces of the several touchscreen self-service advice terminals installed across Waverley and Guildford by the Waverley Citizens Advice "Advice Services Transformation Fund" Lottery-backed project. 

It is principally constructed in AngularJS; the main body of each site consists of a single app.js, which contains a controller for every page and a set of huge $scope statements which define the links available for each route. This needs at some point to be split by page and have the links separated out into a JSON for maintainability, but the refactoring will take time and the project keeps growing. If you're maintaining this, have fun. 

app.css, /partials/buttons.html, /bower_components and /templates are the same across all the folders. Everything else is customised per site to a greater or lesser extent.

Not all locations have their own folder; for clarification on this talk to Alok Agarwal (alok@3rdsectorit.co.uk), who is the server provider and administrator for the kiosk hardware. The folder /attract-loop-images contains the images used for the screensaver on the kiosks; this is slightly location-dependent (the locations have been given the opportunity to request specific images to be included on their terminal) but mostly the same across all kiosks.